import org.junit.Test;
import resources.CalculatorResource;

import javax.validation.constraints.Digits;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate() throws Exception {
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals("201", calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals(399, calculatorResource.sum(expression));

        expression = "300+99+1+1+1+1+1+1+1+1";
        assertEquals(407, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2-2";
        assertEquals(16, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "1*1*300";
        assertEquals(300, calculatorResource.multiplication(expression));

        expression = "2*2*2";
        assertEquals(8, calculatorResource.multiplication(expression));
    }

    @Test
    public void testDivision() throws Exception {
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "7/3";
        assertEquals(2, calculatorResource.division(expression));

        expression = "100/10/2";
        assertEquals(5, calculatorResource.division(expression));
        try{
            expression = "1/0";
            calculatorResource.division(expression);
            assert(false);
        } catch (ArithmeticException e){
            assert(true);
        }
        try{
            expression = "1/1/0";
            calculatorResource.division(expression);
            assert(false);
        } catch (ArithmeticException e){
            assert(true);
        }
        try{
            expression = "0/0";
            calculatorResource.division(expression);
            assert(false);
        } catch (ArithmeticException e){
            assert(true);
        }
    }
}
