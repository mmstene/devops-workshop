package resources;

import dao.GroupChatDAO;
import data.GroupChat;
import data.Message;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

/**
 * GroupChat resource exposed at "/groupchat" path
 */
@Path("/groupchat")
public class GroupChatResource {
    GroupChatDAO groupChatDAO;
    ArrayList<GroupChat> chats;

    public GroupChatResource() {
        groupChatDAO = new GroupChatDAO();
        chats = new ArrayList<>();
    }

    /**
     * GET method to get one groupchat with specified groupChatId
     *
     * @param groupChatId of the chat to GET
     * @return GroupChat
     */
    @GET
    @Path("{groupChatId}")
    @Produces(MediaType.APPLICATION_JSON)
    public GroupChat getGroupChat(@PathParam("groupChatId") int groupChatId) {
        groupChatDAO.getGroupChatMessages(groupChatId);
        groupChatDAO.getGroupChatUsers(groupChatId);
        return groupChatDAO.getGroupChat(groupChatId);
    }

    @GET
    @Path("user/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<GroupChat> getGroupChatByUserId(@PathParam("userId") int userId) {
        chats.add(groupChatDAO.getGroupChat(userId));
        return chats;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public GroupChat postGroupChat(GroupChat groupChat){
        groupChatDAO.addGroupChat(groupChat);
        return groupChat;
    }

    @GET
    @Path("/{groupChatId}/message")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Message> getAllMessages(@PathParam("groupChatId") int groupChatId){
        return groupChatDAO.getGroupChatMessages(groupChatId);
    }

    @POST
    @Path("/{groupChatId}/message")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Message postMessage(@PathParam("groupChatId") int groupChatId, Message message){
        return groupChatDAO.addMessage(groupChatId,message);
    }
}
